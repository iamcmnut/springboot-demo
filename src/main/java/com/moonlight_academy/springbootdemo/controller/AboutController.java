package com.moonlight_academy.springbootdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutController {
	
	private String msg;
	
	public AboutController() {
		this.msg = "Yeah";
	}

	@GetMapping("/about")
	public String viewAbout() {
		
		String[] arr = this.msg.split("|");
		
		return "about";
	}
}
